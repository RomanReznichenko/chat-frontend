import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor (private readonly _tokenService: TokenService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const tokens = this._tokenService.getTokens();
    const modifiedReq = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${tokens.accessToken}`),
    });

    return next.handle(modifiedReq);
  }

}
