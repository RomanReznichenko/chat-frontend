import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, of, switchMap, take, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ITokens } from '../models/tokens.model';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private readonly _user: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  public user: Observable<any> = this._user.asObservable();

  get isAuthenticated(): boolean {
    return this._user.value !== null;
  }

  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _tokenService: TokenService) {
    if (_tokenService.getTokens().accessToken) {
      this.loadUser().pipe(take(1)).subscribe();
    }
  }

  signIn(email: string, password: string): Observable<any> {
    const url = environment.apiUrl + '/authentication/sign-in'
    const body = {
      email,
      password
    };

    return this._httpClient.post<ITokens>(url, body).pipe(
      tap((tokens) => this._tokenService.setTokens(tokens)),
      switchMap(() => this.loadUser()),
      map(() => { return; }));
  }

  signUp(email: string, password: string, nickName: string): Observable<void> {
    const url = environment.apiUrl + '/authentication/sign-up'
    const body = {
      email,
      nickName,
      password
    };

    return this._httpClient.post<ITokens>(url, body).pipe(
      tap((tokens) => this._tokenService.setTokens(tokens)),
      switchMap(() => this.loadUser()),
      map(() => { return; }));
  }

  logOut(): Observable<void> {
    const url = environment.apiUrl + '/authentication/logout'
    return this._httpClient.delete(url).pipe(
      tap(() => this._user.next(null)),
      map(() => { return; }));
  }

  private loadUser(): Observable<void> {
    const url = environment.apiUrl + '/users/me';
    return this._httpClient.get(url).pipe(
      tap((user) => this._user.next(user)),
      map(() => { return; })
    );
  }
}
