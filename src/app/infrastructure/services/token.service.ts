import { Injectable } from '@angular/core';
import { ITokens } from '../models/tokens.model';


const ACCESS_TOKEN_KEY: string = 'acces_token';
const REFRESH_TOKEN_KEY: string = 'refresh_token';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private _tokens: ITokens = {};

  constructor() {
    this.loadTokensFromStorage();
  }

  public getTokens(): ITokens {
    return this._tokens;
  }

  public setTokens(tokens: ITokens): void {
    this.setAccessToken(tokens.accessToken ?? '');
    this.setRefreshToken(tokens.refreshToken ?? '');
  }

  public setAccessToken(token: string): void {
    localStorage.setItem(ACCESS_TOKEN_KEY, token);
    this._tokens.accessToken = token;
  }

  public setRefreshToken(token: string): void {
    localStorage.setItem(REFRESH_TOKEN_KEY, token);
    this._tokens.refreshToken = token;
  }

  public clear(): void {
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    localStorage.removeItem(REFRESH_TOKEN_KEY);
    this._tokens = {};
  }

  private loadTokensFromStorage (): void {
    const accessToken = localStorage.getItem(ACCESS_TOKEN_KEY) ?? undefined;
    const refreshToken = localStorage.getItem(REFRESH_TOKEN_KEY) ?? undefined;

    this._tokens =  {accessToken, refreshToken};
  }
}
