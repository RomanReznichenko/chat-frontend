import { EventEmitter, Injectable, OnDestroy } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { from, Observable, of, tap } from 'rxjs';
import { TokenService } from 'src/app/infrastructure/services/token.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class CommunicationService implements OnDestroy {
  private readonly _connection!: HubConnection;

  public recieveMessage: EventEmitter<any> = new EventEmitter();
  public inviteRequest: EventEmitter<any> = new EventEmitter();
  public inviteResponse: EventEmitter<any> = new EventEmitter();

  constructor(private readonly _tokenService: TokenService) {
    const tokens = _tokenService.getTokens();
    const url = `${environment.socketUrl}/messaging`;
    this._connection = new HubConnectionBuilder()
      .withUrl(url, { accessTokenFactory: () => tokens.accessToken! })
      .build();

      this._connection.start().then(() => this.startListening());
  }

  ngOnDestroy(): void {
    this._connection.stop();
  }

  public sendInviteRequest(addressee: string): Observable<void> {
    return from(this._connection.invoke('InitiateAsync', addressee));
  }

  public sendInviteResponse(addressee: string, response: number): Observable<void> {
    return from(this._connection.invoke('InviteResponse', addressee, response));
  }

  public sendMessage(addressee: string, message: string) {
    return from(this._connection.invoke('SendAsync', addressee, message));
  }

  private startListening(): void {
    this._connection.on('ReceiveMessage', (message) => {
      this.recieveMessage.emit(message);
      console.log('ReceiveMessage', message);
    });

    this._connection.on('InviteRequest', (message) => {
      this.inviteRequest.emit(message);
      console.log('InviteRequest', message);
    });

    this._connection.on('InviteResponse', (message) => {
      this.inviteResponse.emit(message);
      console.log('InviteRequest', message);
    });
  }
}
