import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ActiveUsersService {
  constructor(private readonly _httpClient: HttpClient) { }

  public getActiveUsers(): Observable<any> {
    const url = environment.apiUrl + '/users/active';

    return this._httpClient.get(url);
  }

  public getUserById(userId: string): Observable<any> {
    const url = `${environment.apiUrl}/users/${userId}/active`;

    return this._httpClient.get(url);
  }
}
