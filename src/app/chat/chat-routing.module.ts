import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActiveUsersComponent } from './active-users/active-users.component';
import { MessagingComponent } from './messaging/messaging.component';

const routes: Routes = [
  {
    path: 'active-users',
    component: ActiveUsersComponent
  },
  {
    path: 'messaging/:id',
    component: MessagingComponent
  },
  {
    path: '**',
    redirectTo: 'active-users'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
