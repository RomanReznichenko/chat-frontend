import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { map, merge, Observable, scan, Subject, take, takeUntil, toArray, withLatestFrom } from 'rxjs';
import { AuthenticationService } from 'src/app/infrastructure/services/authentication.service';
import { DestroyService } from 'src/app/infrastructure/services/destroy.service';
import { ActiveUsersService } from '../services/active-users.service';
import { CommunicationService } from '../services/communication.service';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html',
  styleUrls: ['./messaging.component.scss'],
  providers: [DestroyService]
})
export class MessagingComponent implements OnInit {
  messageConrtol: FormControl = new FormControl("");

  messages!: Observable<{ message: string, user: string, isMy: boolean }[]>;
  addressee!: Observable<any>;
  myMessages: Subject<string> = new Subject<string>();

  private addresseeId!: string;

  constructor(
    private readonly _destroy: DestroyService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _authService: AuthenticationService,
    private readonly _activeUsersService: ActiveUsersService,
    private readonly _communicationService: CommunicationService) { }

  ngOnInit(): void {
    this.addresseeId = this._activatedRoute.snapshot.paramMap.get('id')!;
    this.addressee = this._activeUsersService.getUserById(this.addresseeId).pipe(take(1));

    const recievedMessages = this._communicationService.recieveMessage.pipe(
      withLatestFrom(this.addressee),
      map(([message, user]) => {
        return {
          message,
          isMy: false,
          user: user.nickName
        }
      }));

    const myMessages = this.myMessages.pipe(
      withLatestFrom(this._authService.user),
      map(([message, user]) => {
        return {
          message,
          isMy: true,
          user: user.nickName
        }
      }));

    this.messages = merge(recievedMessages, myMessages).pipe(
      scan((acc, cur) => [...acc, cur], [] as any[]),
      takeUntil(this._destroy));

  }

  sendMessage(): void {
    const message = this.messageConrtol.value;
    if (!message) {
      return;
    }

    this._communicationService.sendMessage(this.addresseeId, message)
      .pipe(takeUntil(this._destroy)).subscribe();
    this.myMessages.next(message);
    this.messageConrtol.setValue('');
  }

}
