import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './chat.component';
import { CommunicationService } from './services/communication.service';
import { ActiveUsersComponent } from './active-users/active-users.component';
import { MessagingComponent } from './messaging/messaging.component';
import { ActiveUsersService } from './services/active-users.service';

import { MatIconModule } from '@angular/material/icon'
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { RequestModalComponent } from './request-modal/request-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    ChatComponent,
    ActiveUsersComponent,
    MessagingComponent,
    RequestModalComponent
  ],
  imports: [
    CommonModule,
    ChatRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [
    ActiveUsersService,
    CommunicationService
  ]
})
export class ChatModule { }
