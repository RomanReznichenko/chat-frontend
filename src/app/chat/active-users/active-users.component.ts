import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, take, takeUntil } from 'rxjs';
import { DestroyService } from 'src/app/infrastructure/services/destroy.service';
import { RequestModalComponent } from '../request-modal/request-modal.component';
import { ActiveUsersService } from '../services/active-users.service';
import { CommunicationService } from '../services/communication.service';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.scss'],
  providers: [
    DestroyService
  ]
})
export class ActiveUsersComponent implements OnInit {
  users!: Observable<any>;

  constructor(
    private readonly _router: Router,
    private readonly _dialog: MatDialog,
    private readonly _snackBar: MatSnackBar,
    private readonly _destroy: DestroyService,
    private readonly _activeUsersService: ActiveUsersService,
    private readonly _communicationService: CommunicationService) { }

  ngOnInit(): void {
    this._communicationService.inviteRequest
      .pipe(takeUntil(this._destroy))
      .subscribe((req) => this.handleInvireRequest(req));

    this._communicationService.inviteResponse
      .pipe(takeUntil(this._destroy))
      .subscribe((res) => this.handleInviteResponse(res));

    this.users = this._activeUsersService.getActiveUsers().pipe(take(1));
  }

  public sendInvite(addresse: string): void {
    this._communicationService.sendInviteRequest(addresse).subscribe();
  }

  private handleInvireRequest(request: any): void {
    const dialogRef = this._dialog.open(RequestModalComponent, {
      data: request.nickName
    });

    dialogRef.afterClosed()
      .pipe(takeUntil(this._destroy))
      .subscribe((res) => {
        this._communicationService.sendInviteResponse(request.id, +res);

        if (res) {
          this._router.navigateByUrl(`/chat/messaging/${request.id}`)
        }
      });
  }

  private handleInviteResponse(response: any): void {
    console.log('aaaaa', response);
    if (response.status) {
      this._router.navigateByUrl(`/chat/messaging/${response.sender.id}`)
    }
    else {
      this._snackBar.open(`User ${response.sender.nickName} decline your request.`, undefined, { duration: 3000 });
    }
  }
}
