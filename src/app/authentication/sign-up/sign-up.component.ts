import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs';
import { AuthenticationService } from 'src/app/infrastructure/services/authentication.service';
import { DestroyService } from 'src/app/infrastructure/services/destroy.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  providers: [DestroyService]
})
export class SignUpComponent implements OnInit {
  public signUpForm!: FormGroup;

  constructor(
    private readonly _router: Router,
    private readonly _destroy: DestroyService,
    private readonly _formBuilder: FormBuilder,
    private readonly _authService: AuthenticationService) { }

  ngOnInit(): void {
    this.signUpForm = this._formBuilder.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      nickName: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, Validators.minLength(10)]),
      confirmationPassword: new FormControl("", [Validators.required])
    }, {
      validators: [this.passwordsMismatch('password', 'confirmationPassword')]
    });
  }

  submit(): void {
    if (!this.signUpForm.valid) {
      return;
    }

    const { email, nickName, password } = this.signUpForm.value;

    this._authService.signUp(email, password, nickName)
      .pipe(takeUntil(this._destroy))
      .subscribe(() => {
        this._router.navigateByUrl('/chat');
      })
  }

  private passwordsMismatch(password: string, confirm: string) {
    return (fg: FormGroup): ValidationErrors | null => {
      const passwordControl = fg.controls[password];
      const confirmControl = fg.controls[confirm];

      if (passwordControl.disabled && confirmControl.disabled) {
        return null;
      }

      let isInvalid = false;
      let isBlank = false;

      isInvalid = passwordControl.value !== confirmControl.value;
      isBlank = !passwordControl.value && !confirmControl.value;

      if (isInvalid && !isBlank) {
        confirmControl.setErrors({ passwordsMismatch: true });
      } else {
        if (confirmControl.errors) {
          const errors = Object.entries(confirmControl.errors).filter(([key, _]) => key !== 'passwordsMismatch');
          let errorsObject = null;
          if (errors.length !== 0) {
            errorsObject = errors.reduce((acc: any, [key, val]) => {
              acc[key] = val;

              return acc;
            }, {});
          }

          confirmControl.setErrors(null);
        }

      }

      return isInvalid || isBlank ? { passwordsMismatch: true } : null;
    }
  }
}
