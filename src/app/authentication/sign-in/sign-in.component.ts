import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs';
import { AuthenticationService } from 'src/app/infrastructure/services/authentication.service';
import { DestroyService } from 'src/app/infrastructure/services/destroy.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  providers: [DestroyService]
})
export class SignInComponent implements OnInit {
  public signInForm!: FormGroup;

  constructor(
    private readonly _router: Router,
    private readonly _destroy: DestroyService,
    private readonly _formBuilder: FormBuilder,
    private readonly _authService: AuthenticationService) { }

  ngOnInit(): void {
    this.signInForm = this._formBuilder.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required])
    });
  }


  submit(): void {
    if (!this.signInForm.valid) {
      return;
    }

    const { email, password } = this.signInForm.value;

    this._authService.signIn(email, password)
      .pipe(takeUntil(this._destroy))
      .subscribe(() => {
        this._router.navigateByUrl("/chat");
      });
  }

}
