export const environment = {
  production: true,
  apiUrl: 'https://testassignment20220822100938.azurewebsites.net/api',
  socketUrl: 'https://testassignment20220822100938.azurewebsites.net'
};
