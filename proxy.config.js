const PROXY_CONFIG = [
  {
      context: [
          "/api",
          "/messaging"
      ],
      target: "http://localhost",
      secure: false,
      // changeOrigin: true,
      ws: true
  }
]

module.exports = PROXY_CONFIG;


// {
//   "/api": {
//     "target": "http://localhost",
//     "secure": false,
//     "changeOrigin": true
//   },
//   "/chat": {
//     "target": "http://localhost",
//     "secure": false,
//     "changeOrigin": true,
//     "ws": true
//   }
// }
